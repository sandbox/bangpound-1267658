<?php
/**
 * @file
 * cablegate.views_default.inc
 */

/**
 * Implementation of hook_views_default_views().
 */
function cablegate_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'cablegate_index';
  $view->description = 'Emulates the default Drupal front page; you may set the default home page path to this view to make it your front page.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'cablegate_index';
  $view->core = 0;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_classification' => 'field_classification',
    'field_created' => 'field_created',
    'field_origin' => 'field_origin',
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = 'field_created';
  $handler->display->display_options['style_options']['info'] = array(
    'field_classification' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'field_created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
    ),
    'field_origin' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: Content: Classification */
  $handler->display->display_options['fields']['field_classification']['id'] = 'field_classification';
  $handler->display->display_options['fields']['field_classification']['table'] = 'field_data_field_classification';
  $handler->display->display_options['fields']['field_classification']['field'] = 'field_classification';
  $handler->display->display_options['fields']['field_classification']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_classification']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_classification']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_classification']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_classification']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_classification']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_classification']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_classification']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_classification']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_classification']['group_column'] = 'value';
  $handler->display->display_options['fields']['field_classification']['field_api_classes'] = 0;
  /* Field: Content: Created date */
  $handler->display->display_options['fields']['field_created']['id'] = 'field_created';
  $handler->display->display_options['fields']['field_created']['table'] = 'field_data_field_created';
  $handler->display->display_options['fields']['field_created']['field'] = 'field_created';
  $handler->display->display_options['fields']['field_created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_created']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_created']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_created']['field_api_classes'] = 0;
  /* Field: Content: Origin */
  $handler->display->display_options['fields']['field_origin']['id'] = 'field_origin';
  $handler->display->display_options['fields']['field_origin']['table'] = 'field_data_field_origin';
  $handler->display->display_options['fields']['field_origin']['field'] = 'field_origin';
  $handler->display->display_options['fields']['field_origin']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_origin']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_origin']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_origin']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_origin']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_origin']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_origin']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_origin']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_origin']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_origin']['group_column'] = 'value';
  $handler->display->display_options['fields']['field_origin']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Created date (field_created) */
  $handler->display->display_options['filters']['field_created_value']['id'] = 'field_created_value';
  $handler->display->display_options['filters']['field_created_value']['table'] = 'field_data_field_created';
  $handler->display->display_options['filters']['field_created_value']['field'] = 'field_created_value';
  $handler->display->display_options['filters']['field_created_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_created_value']['expose']['operator_id'] = 'field_created_value_op';
  $handler->display->display_options['filters']['field_created_value']['expose']['label'] = 'Date';
  $handler->display->display_options['filters']['field_created_value']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['field_created_value']['expose']['operator'] = 'field_created_value_op';
  $handler->display->display_options['filters']['field_created_value']['expose']['identifier'] = 'field_created_value';
  $handler->display->display_options['filters']['field_created_value']['expose']['required'] = 0;
  $handler->display->display_options['filters']['field_created_value']['expose']['multiple'] = FALSE;
  /* Filter criterion: Content: Reference ID (field_reference_id) */
  $handler->display->display_options['filters']['field_reference_id_value']['id'] = 'field_reference_id_value';
  $handler->display->display_options['filters']['field_reference_id_value']['table'] = 'field_data_field_reference_id';
  $handler->display->display_options['filters']['field_reference_id_value']['field'] = 'field_reference_id_value';
  $handler->display->display_options['filters']['field_reference_id_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_reference_id_value']['expose']['operator_id'] = 'field_reference_id_value_op';
  $handler->display->display_options['filters']['field_reference_id_value']['expose']['label'] = 'Reference ID';
  $handler->display->display_options['filters']['field_reference_id_value']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['field_reference_id_value']['expose']['operator'] = 'field_reference_id_value_op';
  $handler->display->display_options['filters']['field_reference_id_value']['expose']['identifier'] = 'field_reference_id_value';
  $handler->display->display_options['filters']['field_reference_id_value']['expose']['required'] = 0;
  $handler->display->display_options['filters']['field_reference_id_value']['expose']['multiple'] = FALSE;
  /* Filter criterion: Content: References (field_references) */
  $handler->display->display_options['filters']['field_references_value']['id'] = 'field_references_value';
  $handler->display->display_options['filters']['field_references_value']['table'] = 'field_data_field_references';
  $handler->display->display_options['filters']['field_references_value']['field'] = 'field_references_value';
  $handler->display->display_options['filters']['field_references_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_references_value']['expose']['operator_id'] = 'field_references_value_op';
  $handler->display->display_options['filters']['field_references_value']['expose']['label'] = 'References';
  $handler->display->display_options['filters']['field_references_value']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['field_references_value']['expose']['operator'] = 'field_references_value_op';
  $handler->display->display_options['filters']['field_references_value']['expose']['identifier'] = 'field_references_value';
  $handler->display->display_options['filters']['field_references_value']['expose']['required'] = 0;
  $handler->display->display_options['filters']['field_references_value']['expose']['multiple'] = FALSE;
  /* Filter criterion: Content: Classification (field_classification) */
  $handler->display->display_options['filters']['field_classification_tid']['id'] = 'field_classification_tid';
  $handler->display->display_options['filters']['field_classification_tid']['table'] = 'field_data_field_classification';
  $handler->display->display_options['filters']['field_classification_tid']['field'] = 'field_classification_tid';
  $handler->display->display_options['filters']['field_classification_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_classification_tid']['expose']['operator_id'] = 'field_classification_tid_op';
  $handler->display->display_options['filters']['field_classification_tid']['expose']['label'] = 'Classification';
  $handler->display->display_options['filters']['field_classification_tid']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['field_classification_tid']['expose']['operator'] = 'field_classification_tid_op';
  $handler->display->display_options['filters']['field_classification_tid']['expose']['identifier'] = 'field_classification_tid';
  $handler->display->display_options['filters']['field_classification_tid']['expose']['multiple'] = 1;
  $handler->display->display_options['filters']['field_classification_tid']['expose']['reduce'] = 0;
  $handler->display->display_options['filters']['field_classification_tid']['reduce_duplicates'] = 0;
  $handler->display->display_options['filters']['field_classification_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_classification_tid']['vocabulary'] = 'classifications';
  $handler->display->display_options['filters']['field_classification_tid']['error_message'] = 1;
  /* Filter criterion: Search: Search Terms */
  $handler->display->display_options['filters']['keys']['id'] = 'keys';
  $handler->display->display_options['filters']['keys']['table'] = 'search_index';
  $handler->display->display_options['filters']['keys']['field'] = 'keys';
  $handler->display->display_options['filters']['keys']['exposed'] = TRUE;
  $handler->display->display_options['filters']['keys']['expose']['operator_id'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['label'] = 'Search Terms';
  $handler->display->display_options['filters']['keys']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['keys']['expose']['operator'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['identifier'] = 'keys';
  $handler->display->display_options['filters']['keys']['expose']['multiple'] = FALSE;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'frontpage';

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Front page feed';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['path'] = 'rss.xml';
  $handler->display->display_options['displays'] = array(
    'default' => 'default',
    'page' => 'page',
  );
  $handler->display->display_options['sitename_title'] = '1';
  $export['cablegate_index'] = $view;

  return $export;
}
