<?php
/**
 * @file
 * cablegate.feeds_importer_default.inc
 */

/**
 * Implementation of hook_feeds_importer_default().
 */
function cablegate_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'cables_csv';
  $feeds_importer->config = array(
    'name' => 'Cables CSV',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 1,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'CablegateCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'cable',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => '0',
            'target' => 'guid',
            'unique' => 0,
          ),
          1 => array(
            'source' => '1',
            'target' => 'field_created:start',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '2',
            'target' => 'field_reference_id',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => '3',
            'target' => 'field_origin',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '4',
            'target' => 'field_classification',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '5',
            'target' => 'field_references',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => '6',
            'target' => 'field_header',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => '7',
            'target' => 'body',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'subject',
            'target' => 'title',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'tags',
            'target' => 'field_tags',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['cables_csv'] = $feeds_importer;

  return $export;
}
