<?php

/**
 * Rewrites imported rows.
 */
function cablegate_feeds_after_parse(FeedsSource $source, FeedsParserResult $result) {
  if ($source->id == 'cables_csv') {
    foreach ($result as &$rows) {
      if (is_array($rows)) {
        foreach ($rows as &$row) {

          // Classifications have modifiers.
          $row[4] = array_filter(explode('/', $row[4]));

          // References are | separated values.
          $row[5] = explode('|', $row[5]);

          // Cable body is escaped.
          $row[7] = stripcslashes($row[7]);

          // Find possibly multi line subject.
          $matches = array();
          preg_match('|^subj.*?:(.+?)$|im', $row[7], $matches);
          $subject = trim($matches[1]);
          $row['subject'] = empty($subject) ? $row[2] : $subject;

          // Find tags which may be space or comma separated, also multi line.
          $matches = array();
          preg_match('|^tags.*?:(.+?)$|im', $row[7], $matches);
          $row['tags'] = preg_split("/[\s,]+/", trim($matches[1]));
        }
      }
    }
  }
}
