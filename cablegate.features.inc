<?php
/**
 * @file
 * cablegate.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function cablegate_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "plugins") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function cablegate_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_node_info().
 */
function cablegate_node_info() {
  $items = array(
    'cable' => array(
      'name' => t('Cable'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
