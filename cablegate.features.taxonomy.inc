<?php
/**
 * @file
 * cablegate.features.taxonomy.inc
 */

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function cablegate_taxonomy_default_vocabularies() {
  return array(
    'classifications' => array(
      'name' => 'Classifications',
      'machine_name' => 'classifications',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'missions' => array(
      'name' => 'Missions',
      'machine_name' => 'missions',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'tags' => array(
      'name' => 'Tags',
      'machine_name' => 'tags',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
